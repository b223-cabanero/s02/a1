<?php 
  header("Content-type: text/css");
?>

.container {
  width: 25rem;
  margin: auto;
  margin-top: 5rem;
  padding: 0.5rem 2rem;
  box-shadow: 0 2px 10px 0 rgb(0 0 0 / 12%);
  border-radius: 10px;
}

.container:nth-child(2) {
  margin-top: 2rem;
}
