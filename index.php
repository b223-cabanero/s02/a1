<?php include_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>WDC039-2 | Selection Control Structures and Array Manipulation</title>
  <link rel="stylesheet" href="./style.php" media="screen">
</head>

<body>
  <div class="container">
    <h1>Divisible by Five</h1>
    <p>
      <?php printDivisibleOfFive(); ?>
    </p>
  </div>

  <div class="container">
    <h1>Array Manipulation</h1>
    <?php array_push($students, "Vilmar C."); ?>
    <pre>
      <?php echo "Students: "; print_r($students); ?>
    </pre>

    <pre>
      <?php echo "Number of students: "; print_r(count($students)); ?>
    </pre>

    <br> <hr> <br>

    <?php array_push($students, "Anjela D."); ?>

    <pre>
      <?php echo "Students: "; print_r($students); ?>
    </pre>

    <pre>
      <?php echo "Number of students: "; print_r(count($students)); ?>
    </pre>

    <br> <hr> <br>

    <?php array_shift($students); ?>

    <pre>
      <?php echo "Students: "; print_r($students); ?>
    </pre>

    <pre>
      <?php echo "Number of students: "; print_r(count($students)); ?>
    </pre>

  </div>
</body>

</html>